package com.oo.bootcamp;

import java.util.HashMap;
import java.util.Objects;

public class ParkingLot {

    private final HashMap<Token, Car> cars;

    private final int capacity;

    public ParkingLot(int capacity) {
        this.capacity = capacity;
        this.cars = new HashMap<>();
    }

    public Token park(Car car) {
        if (Objects.isNull(car)) {
            throw new InvalidCarException();
        }
        if (capacity - cars.size() == 0) {
            throw new NoSpaceException();
        }
        Token token = new Token();
        cars.put(token, car);
        return token;
    }

    public Car get(Token token) {

        if (!cars.containsKey(token)) {
            throw new InvalidTokenException();
        }
        return cars.remove(token);
    }

    public int getLeftSpace() {
        return capacity - cars.size();
    }
}
