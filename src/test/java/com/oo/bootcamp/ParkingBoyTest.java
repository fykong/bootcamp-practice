package com.oo.bootcamp;

public class ParkingBoyTest {
    /*
      1. boy只有一个停车场，停车场有空位，顺利停车，拿到token
      2. boy有2个停车场，停车场都有空位，顺利停车，拿到token
      3. boy有2个停车场，第一个停车场满，停到第二个停车场，拿到token
      4. boy有2个停车场，停车场全满，无法停车
      <p>
      5. boy只有一个停车场，车停到停车场，拿正确token顺利取车
      6. boy有2个停车场，车停到第二个停车场，拿正确token顺利取车
      7. boy有一个停车场，车停到停车场，拿空的token无法取到车
      8. boy有一个停车场，车停到停车场，拿用过的token无法取到车
      9. boy有一个停车场，车停到停车场，拿错误的token无法取到车
     */
}
