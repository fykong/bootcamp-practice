package com.oo.bootcamp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingLotTest {

    @Test
    void should_return_a_token_when_park_car_into_parking_lot() {
        //Given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car();
        //When
        Token token = parkingLot.park(car);
        //Then
        assertNotNull(token);
    }

    @Test
    void should_return_no_park_space_when_park_into_full_parking_lot() {
        //Given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car();
        parkingLot.park(car);

        //when
        Assertions.assertThrows(NoSpaceException.class, () -> parkingLot.park(new Car()));
    }

    @Test
    void should_return_token_when_park_second_car_into_parking_lot() {
        //Given
        ParkingLot parkingLot = new ParkingLot(2);
        Token firstToken = parkingLot.park(new Car());
        Car car = new Car();
        //When
        Token token = parkingLot.park(car);
        //Then
        assertNotNull(token);
        assertNotSame(firstToken, token);
    }

    @Test
    void should_no_get_token_when_park_invalid_car() {
        ParkingLot parkingLot = new ParkingLot(1);

        assertThrows(InvalidCarException.class, () -> parkingLot.park(null));
    }

    @Test
    void should_get_my_car_when_pick_with_my_token() {
        //Given
        ParkingLot parkingLot = new ParkingLot(1);
        Car myCar = new Car();
        Token myToken = parkingLot.park(myCar);
        //When
        Car pickedCar = parkingLot.get(myToken);
        //Then
        assertSame(myCar, pickedCar);
    }

    @Test
    void should_cannot_get_car_when_pick_with_wrong_token() {
        //Given
        ParkingLot parkingLot = new ParkingLot(1);
        parkingLot.park(new Car());
        Token token = new Token();
        //when
        assertThrows(InvalidTokenException.class, () -> parkingLot.get(token));
    }

    @Test
    void should_cannot_get_car_when_pick_with_used_token() {
        ParkingLot parkingLot = new ParkingLot(1);
        Token token = parkingLot.park(new Car());
        parkingLot.get(token);

        assertThrows(InvalidTokenException.class, () -> parkingLot.get(token));
    }

    @Test
    void should_return_left_space_given_valid_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(1);

        assertEquals(1, parkingLot.getLeftSpace());
    }

    @Test
    void should_return_left_space_when_park_car_into_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(1);
        parkingLot.park(new Car());

        assertEquals(0, parkingLot.getLeftSpace());
    }


}
